<?php

require_once('phpmailer/PHPMailerAutoload.php');

$toemails = array();

$toemails[] = array(
				'email' => 'admin@pwmcenter.com', // Your Email Address
				'name' => 'Open account Request' // Your Name
			);

// Form Processing Messages
$message_success = 'We have <strong>successfully</strong> received your request and will get Back to you as soon as possible.';

// Add this only if you use reCaptcha with your Contact Forms
$recaptcha_secret = ''; // Your reCaptcha Secret

$mail = new PHPMailer();

// If you intend you use SMTP, add your SMTP Code after this Line

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	if( $_POST['template-contactform-email2'] != '' ) {

		$name 				= isset( $_POST['template-contactform-name'] ) 				? $_POST['template-contactform-name'] : '';
		$surname 			= isset( $_POST['template-contactform-surname'] ) 			? $_POST['template-contactform-surname'] : '';
		$birthday 			= isset( $_POST['template-contactform-birth-day'] ) 		? $_POST['template-contactform-birth-day'] : '';
		$birthmonth 		= isset( $_POST['template-contactform-birth-month'] ) 		? $_POST['template-contactform-birth-month'] : '';
		$birthyear 			= isset( $_POST['template-contactform-birth-year'] ) 		? $_POST['template-contactform-birth-year'] : '';
		$idtype 			= isset( $_POST['template-contactform-idtype'] ) 			? $_POST['template-contactform-idtype'] : '';		
		$idno 				= isset( $_POST['template-contactform-idno'] ) 				? $_POST['template-contactform-idno'] : '';
		$gender 			= isset( $_POST['template-contactform-gender'] ) 			? $_POST['template-contactform-gender'] : '';
		$nationality 		= isset( $_POST['template-contactform-nationality'] ) 		? $_POST['template-contactform-nationality'] : '';
		$citizenship 		= isset( $_POST['template-contactform-citizenship'] ) 		? $_POST['template-contactform-citizenship'] : '';
		$adress 			= isset( $_POST['template-contactform-adress'] ) 			? $_POST['template-contactform-adress'] : '';
		$city 				= isset( $_POST['template-contactform-city'] ) 				? $_POST['template-contactform-city'] : '';
		$state 				= isset( $_POST['template-contactform-state'] ) 			? $_POST['template-contactform-state'] : '';
		$zipcode 			= isset( $_POST['template-contactform-zipcode'] ) 			? $_POST['template-contactform-zipcode'] : '';
		$country 			= isset( $_POST['template-contactform-country'] ) 			? $_POST['template-contactform-country'] : '';
		$offtelehonecode 	= isset( $_POST['template-contactform-offtelehone-code'] ) 	? $_POST['template-contactform-offtelehone-code'] : '';
		$offtelehone 		= isset( $_POST['template-contactform-offtelehone'] ) 		? $_POST['template-contactform-offtelehone'] : '';
		$mobilecall 		= isset( $_POST['template-contactform-offtelehone-mobile'] ) 		? $_POST['template-contactform-offtelehone-mobile'] : '';
		$mobile 			= isset( $_POST['template-contactform-mobile'] ) 			? $_POST['template-contactform-mobile'] : '';
		$email2 			= isset( $_POST['template-contactform-email2'] ) 			? $_POST['template-contactform-email2'] : '';
		$email3 			= isset( $_POST['template-contactform-email3'] ) 			? $_POST['template-contactform-email3'] : '';
		$amount8 			= isset( $_POST['template-contactform-amount8'] ) 			? $_POST['template-contactform-amount8'] : '';
		$funtransfdate 		= isset( $_POST['template-contactform-funtransfdate'] ) 	? $_POST['template-contactform-funtransfdate'] : '';
		$remibank 			= isset( $_POST['template-contactform-remibank'] ) 			? $_POST['template-contactform-remibank'] : '';
		$checkbox95 		= isset( $_POST['field94'] ) 								? $_POST['field94'] : '';		
		$ref1 				= isset( $_POST['template-contactform-ref1'] ) 				? $_POST['template-contactform-ref1'] : '';
		$templatecall1 		= isset( $_POST['template-contactform-telref1-call'] ) 			? $_POST['template-contactform-telref1-call'] : '';
		$telref1 			= isset( $_POST['template-contactform-telref1'] ) 			? $_POST['template-contactform-telref1'] : '';
		$ref2 				= isset( $_POST['template-contactform-ref2'] ) 				? $_POST['template-contactform-ref2'] : '';
		$templatecall2 		= isset( $_POST['template-contactform-telref2-call'] ) 			? $_POST['template-contactform-telref2-call'] : '';
		$telref2 			= isset( $_POST['template-contactform-telref2'] ) 			? $_POST['template-contactform-telref2'] : '';
		$ref3 				= isset( $_POST['template-contactform-ref3'] ) 				? $_POST['template-contactform-ref3'] : '';
		$templatecall3 		= isset( $_POST['template-contactform-telref3-call'] ) 			? $_POST['template-contactform-telref3-call'] : '';
		$telref3 			= isset( $_POST['template-contactform-telref3'] ) 			? $_POST['template-contactform-telref3'] : '';
		$checkbox96 		= isset( $_POST['field95'] ) 								? $_POST['field95'] : '';		
		$ben1 				= isset( $_POST['template-contactform-ben1'] ) 				? $_POST['template-contactform-ben1'] : '';
		$perben1 			= isset( $_POST['template-contactform-perben1'] ) 			? $_POST['template-contactform-perben1'] : '';
		$ben2 				= isset( $_POST['template-contactform-ben2'] ) 				? $_POST['template-contactform-ben2'] : '';
		$perben2 			= isset( $_POST['template-contactform-perben2'] ) 			? $_POST['template-contactform-perben2'] : '';
		$ben3 				= isset( $_POST['template-contactform-ben3'] ) 				? $_POST['template-contactform-ben3'] : '';
		$perben3 			= isset( $_POST['template-contactform-perben3'] ) 			? $_POST['template-contactform-perben3'] : '';
		$ben4 				= isset( $_POST['template-contactform-ben4'] ) 				? $_POST['template-contactform-ben4'] : '';
		$perben4 			= isset( $_POST['template-contactform-perben4'] ) 			? $_POST['template-contactform-perben4'] : '';
		$ben5 				= isset( $_POST['template-contactform-ben5'] ) 				? $_POST['template-contactform-ben5'] : '';
		$perben5 			= isset( $_POST['template-contactform-perben5'] ) 			? $_POST['template-contactform-perben5'] : '';
		$ip 	= isset( $_POST['ip'] ) ? $_POST['ip'] : '';
		

		$subject = isset($subject) ? $subject : 'New Message From Open Account service';

		$botcheck = $_POST['template-contactform-botcheck'];

		if( $botcheck == '' ) {

			$mail->SetFrom( $email2 , $name );
			$mail->AddReplyTo( $email2 , $name );
			foreach( $toemails as $toemail ) {
				$mail->AddAddress( $toemail['email'] , $toemail['name'] );
			}
			$mail->Subject = $subject;

			$setname = isset($name) ? "Name: $name<br><br>" : '';
			$setsurname = isset($surname) ? "Surname: $surname<br><br>" : '';
			$setIp = isset($ip) ? "IP: $ip<br><br>" : '';

			$setbirthday = 	isset($birthday) ? "$birthday" : '';
			$setbirthmonth = 	isset($birthmonth) ? "$birthmonth" : '';
			$setbirthyear = 	isset($birthyear) ? "$birthyear" : '';

			$setbirth = 'Birth Date: '.$setbirthday.'-'.$setbirthmonth.'-'.$setbirthyear.'<br><br>';

			$setidtype = isset($idtype) ? "Id Type: $idtype<br><br>" : '';			

			$setidno = isset($idno) ? "Id no: $idno<br><br>" : '';

			$setgender = isset($gender) ? "Gender: $gender<br><br>" : '';

			$setnationality = isset($nationality) ? "Nationality: $nationality<br><br>" : '';

			$setcitizenship = isset($citizenship) ? "Citizenship: $citizenship<br><br>" : '';

			$setadress = isset($adress) ? "Adress: $adress<br><br>" : '';

			$setcity = isset($city) ? "City: $city<br><br>" : '';

			$setstate = isset($state) ? "State: $state<br><br>" : '';

			$setzipcode = isset($zipcode) ? "Zipcode: $zipcode<br><br>" : '';

			$setcountry = isset($country) ? "Country: $country<br><br>" : '';

			$setofftelehonecode = isset($offtelehonecode) ? "$offtelehonecode" : '';

			$setofftelehone = isset($offtelehone) ? "$offtelehone<br><br>" : '';

			$setofficetel = 'Office telephone : '.$setofftelehonecode.' '.$setofftelehone;

			$setmobilecall = isset($mobilecall) ? "$mobilecall" : '';

			$setmobile = isset($mobile) ? "$mobile<br><br>" : '';
			
			$setmobiletel = 'Mobile telephone : '.$setmobilecall.' '.$setmobile;

			$setemail2 = isset($email2) ? "Email 2: $email2<br><br>" : '';

			$setemail3 = isset($email3) ? "Email 3: $email3<br><br>" : '';

			$setamount = isset($amount8) ? "Account amount: $amount8<br><br>" : '';

			$setfuntransfdate = isset($funtransfdate) ? "Fund Transf Date: $funtransfdate<br><br>" : '';

			$setremibank = isset($remibank) ? "remibank: $remibank<br><br>" : '';

			$setcheckbox95 = isset($checkbox95) ? "Check Box 95: $checkbox95<br><br>" : '';

			$setref1 = isset($ref1) ? "Ref1: $ref1<br><br>" : '';
			$settemplatecall1 = isset($templatecall1) ? "$templatecall1" : "";
			$settelref1 = isset($telref1) ? "Tel Ref1: ".$settemplatecall1." $telref1<br><br>" : '';

			$setref2 = isset($ref2) ? "Ref2: $ref2<br><br>" : '';
			$settemplatecall2 = isset($templatecall2) ? "$templatecall2" : "";
			$settelref2 = isset($telref2) ? "Tel Ref2: ".$settemplatecall2." $telref2<br><br>" : '';

			$setref3 = isset($ref3) ? "Ref3: $ref3<br><br>" : '';
			$settemplatecall3 = isset($templatecall3) ? "$templatecall3" : "";
			$settelref3 = isset($telref3) ? "Tel Ref3: ".$settemplatecall3." $telref3<br><br>" : '';

			$setcheckbox96 = isset($checkbox96) ? "Check Box96: $checkbox96<br><br>" : '';

			$setben1 = isset($ben1) ? "Ben1: $ben1<br><br>" : '';

			$setperben1 = isset($perben1) ? "Perben1: $perben1<br><br>" : '';

			$setben2 = isset($ben2) ? "Ben2: $ben2<br><br>" : '';

			$setperben2 = isset($perben2) ? "Perben2: $perben2<br><br>" : '';

			$setben3 = isset($ben3) ? "Ben3: $ben3<br><br>" : '';

			$setperben3 = isset($perben3) ? "Perben3: $perben3<br><br>" : '';

			$setben4 = isset($ben4) ? "Ben4: $ben4<br><br>" : '';

			$setperben4 = isset($perben4) ? "Perben4: $perben4<br><br>" : '';

			$setben5 = isset($ben5) ? "Ben5: $ben5<br><br>" : '';

			$setperben5 = isset($perben5) ? "Perben5: $perben5<br><br>" : '';
			
			
	
			$referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

			$body = "$setname $setsurname $setIp $setbirth $setidtype $setemail $setidno $setgender $setnationality $setcitizenship $setadress $setcity $setstate $setzipcode $setcountry $setofficetel $setmobiletel $setemail2 $setemail3 $setamount $setfuntransfdate $setremibank $setcheckbox95 $setref1 $settelref1 $setref2 $settelref2 $setref3 $settelref3 $setcheckbox96 $setben1 $setperben1 $setben2 $setperben2 $setben3 $setperben3 $setben4 $setperben4 $setben5 $setperben5 $service $message $referrer";

			// Runs only when File Field is present in the Contact Form
			if ( isset( $_FILES['template-contactform-file'] ) && $_FILES['template-contactform-file']['error'] == UPLOAD_ERR_OK ) {
				$mail->IsHTML(true);
				$mail->AddAttachment( $_FILES['template-contactform-file']['tmp_name'], $_FILES['template-contactform-file']['name'] );
			}
			if ( isset( $_FILES['template-contactform-file2'] ) && $_FILES['template-contactform-adressproof']['error'] == UPLOAD_ERR_OK ) {
				$mail->IsHTML(true);
				$mail->AddAttachment( $_FILES['template-contactform-file2']['tmp_name'], $_FILES['template-contactform-file2']['name'] );
			}

			// Runs only when reCaptcha is present in the Contact Form
			//if( isset( $_POST['g-recaptcha-response'] ) ) {
				//$recaptcha_response = $_POST['g-recaptcha-response'];
				//$response = file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=6Ld2LTMUAAAAAF36UKJuU7zFVgjKJIBZvLvTK722" . $recaptcha_secret . "&response=" . $recaptcha_response );

				//$g_response = json_decode( $response );

				//if ( $g_response->success !== true ) {
					//echo '{ "alert": "error", "message": "Captcha not Validated! Please Try Again." }';
					//die;
				//}
			//}

			// Uncomment the following Lines of Code if you want to Force reCaptcha Validation

			// if( !isset( $_POST['g-recaptcha-response'] ) ) {
			// 	echo '{ "alert": "error", "message": "Captcha not Submitted! Please Try Again." }';
			// 	die;
			// }

			$mail->MsgHTML( $body );
			$sendEmail = $mail->Send();

			if( $sendEmail == true ):
				echo '{ "alert": "success", "message": "' . $message_success . '" }';
			else:
				echo '{ "alert": "error", "message": "Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br /><br /><strong>Reason:</strong><br />' . $mail->ErrorInfo . '" }';
			endif;
		} else {
			echo '{ "alert": "error", "message": "Bot <strong>Detected</strong>.! Clean yourself Botster.!" }';
		}
	} else {
		echo '{ "alert": "error", "message": "Please <strong>Fill up</strong> all the Fields and Try Again." }';
	}
} else {
	echo '{ "alert": "error", "message": "An <strong>unexpected error</strong> occured. Please Try Again later." }';
}

?>